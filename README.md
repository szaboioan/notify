Notifier
==========
This is a work in progress, there is no release version for now.

### Install with

Clone repository, run npm install in project root directory.
> There is no way to install it through npm for now.

Browser support
==========
Only modern browsers that support ES6.
 
Dependencies 
==========
Check package.json file.


Documentation
==========

index.js file exports the following functions:
```js
initWithCloseButton(position)
initWithTimer(position, seconds = 3)

addDanger(text)
addInfo(text)
addSuccess(text)
addWarning(text)
```

Bellow is a use-case that add a notice with a close button.
The notice will disappear only when close button is actioned.

```js
initWithCloseButton(position)

addDanger(text)
```

>Init function should be called once and then all notices will be added / removed
as it dictates.
