import ArrayHelper from "../../src/util/Array"

test('Diff A - B', () => {
    let A = [2, 3, 4, 5, 6]
    let B = [3, 4, 6, 7]
    let expected = [2, 5]

    let actual = ArrayHelper.diff(A, B)

    expect(expected).toStrictEqual(actual)
})

test('Diff B - A', () => {
    let A = [2, 3, 4, 5, 6]
    let B = [3, 4, 6, 7]
    let expected = [7]

    let actual = ArrayHelper.diff(B, A)

    expect(expected).toStrictEqual(actual)
})

