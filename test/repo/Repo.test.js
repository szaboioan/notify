import InvalidItemError from "../../src/InvlidItemError"
import {Notice} from "../../src/Notice"
import getRepo from "../../src/Repo"

test('Add an invalid item throws error', () => {
    let repo = getRepo()
    try {
        repo.add({})
        expect(true).toBe(false)
    } catch (err) {
        expect(err).toBeInstanceOf(InvalidItemError)
    }
})

test('Add multiple items to repo', () => {
    let repo = getRepo()
    let itemOne = new Notice('success', 'Item one success', true)
    let itemTwo = new Notice('danger', 'Item two danger', true)
    repo.add(itemOne)
    repo.add(itemTwo)
    expect(repo.getLength()).toBe(2)
})

test('Find second notice by id', () => {
    let repo = getRepo()
    let itemOne = new Notice('success', 'Item one success', true)
    let itemTwo = new Notice('danger', 'Item two danger', true)
    let itemThree = new Notice('info', 'Item three danger', true)

    repo.add(itemOne)
    repo.add(itemTwo)
    repo.add(itemThree)

    let notice = repo.find(1)
    expect(notice).toEqual(itemTwo)
})

test('Remove second notice by id', () => {
    let repo = getRepo()
    let itemOne = new Notice('success', 'Item one success', true)
    let itemTwo = new Notice('danger', 'Item two danger', true)
    let itemThree = new Notice('info', 'Item three danger', true)

    repo.add(itemOne)
    repo.add(itemTwo)
    repo.add(itemThree)

    repo.remove(itemTwo.id)
    let foundItemTwo = repo.find(1)
    expect(foundItemTwo).toBeNull()
})

test('Remove not existent notice by id', () => {
    let repo = getRepo()
    let itemOne = new Notice('success', 'Item one success', true)
    let itemTwo = new Notice('danger', 'Item two danger', true)

    repo.add(itemOne)
    repo.add(itemTwo)

    let lengthBeforeRemoval = repo.getLength()
    repo.remove(2)
    let lengthAfterRemoval = repo.getLength()

    expect(lengthAfterRemoval).toBe(lengthBeforeRemoval)
})
