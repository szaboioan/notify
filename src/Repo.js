import {Notice} from "./Notice"
import InvalidItemError from "./InvlidItemError"

class Repo {

    constructor() {
        /**
         * @type {[Notice]}
         */
        this.notices = []
        /**
         * @type {number}
         */
        this.currentIndex = 0
    }

    /**
     * @param {Notice}notice
     */
    add(notice) {
        if (!(notice instanceof Notice)) {
            throw new InvalidItemError('Invalid notice provided')
        }

        notice.id = this.currentIndex
        this.notices.push(notice)
        this.currentIndex++
    }


    /**
     * @param id
     * @returns {Notice|null}
     */
    find(id) {
        let foundNotice = this.notices.find(function (notice) {
            return notice.id === id
        })

        if (foundNotice) {
            return foundNotice
        }

        return null
    }

    /**
     * @returns {int}
     */
    getLength() {
        return this.notices.length
    }

    /**
     * @returns {[Notice]}
     */
    getNotices() {
        return this.notices
    }

    /**
     * @param {number}id
     */
    remove(id) {
        let foundIndex = this.notices.findIndex(function (notice) {
            return notice.id === id
        })

        if (foundIndex > -1) {
            this.notices.splice(foundIndex, 1)
        }
    }
}

/**
 * @returns {Repo}
 */
export default function makeRepo() {
    return new Repo()
}
