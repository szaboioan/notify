export default class InvalidItemError extends Error {

    /**
     * @param {string}message
     */
    constructor(message) {
        super();
        this.message = message;
    }
}
