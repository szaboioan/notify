import '../assets/css/basic.css'
import makeRepo from "./Repo"
import {makeUi} from "./ui/Ui"
import {makeNotice} from "./Notice"
import * as types from './const/NoticesTypes'
import * as positions from './const/Positions'
import setCloseEvent from "./event/SetCloseEvent"
import InvalidPositionError from "./InvalidPositionError"
import InitializationError from "./InitializationError"

let body = document.querySelector('body')
let container = document.createElement('div')
container.classList.add('toasters-container')
body.prepend(container)

let repo = makeRepo()
let closeButton = true
let reactUi = null
let initialized = false

export {positions}

/**
 * @param {string}position
 * @throws InitializationError
 */
export function initWithCloseButton(position) {
    if (initialized) {
        throw new InitializationError('Notifier is already initialized')
    }
    if (Object.values(positions).indexOf(position) === -1) {
        throw new InvalidPositionError('Invalid position')
    }
    container.classList.add('toasters-container-' + position)
    closeButton = true
    if (!reactUi) {
        reactUi = makeUi(container, 0)
    }
    setCloseEvent(container, reactUi, repo)

    initialized = true
}

/**
 * @param {string}position
 * @param {int}seconds - Number of seconds after which notice will disappear.
 * @throws InitializationError
 */
export function initWithTimer(position, seconds = 3) {
    if (initialized) {
        throw new InitializationError('Notifier is already initialized')
    }

    if (Object.values(positions).indexOf(position) === -1) {
        throw new InvalidPositionError('Invalid position')
    }
    container.classList.add('toasters-container-' + position)
    closeButton = false
    if (!reactUi) {
        reactUi = makeUi(container, seconds)
    }

    initialized = true
}


/**
 * @param {string}text
 */
export function addDanger(text) {
    let notice = makeNotice(types.DANGER, text, closeButton)
    repo.add(notice)
    reactUi(repo.getNotices())
}

/**
 * @param {string}text
 */
export function addInfo(text) {
    let notice = makeNotice(types.INFO, text, closeButton)
    repo.add(notice)
    reactUi(repo.getNotices())
}

/**
 * @param {string}text
 */
export function addSuccess(text) {
    let notice = makeNotice(types.SUCCESS, text, closeButton)
    repo.add(notice)
    reactUi(repo.getNotices())
}

/**
 * @param {string}text
 */
export function addWarning(text) {
    let notice = makeNotice(types.WARNING, text, closeButton)
    repo.add(notice)
    reactUi(repo.getNotices())
}

initWithTimer(positions.topRight, 4)
addInfo('This is a info note')
addDanger('This is a danger note')
addSuccess('This is a success note')
addWarning('This should be a warning note')
