/**
 * @param {Repo} repo
 * @param {Notice} notice
 */
export default function addNoticeToRepo(repo, notice) {
    repo.add(notice);
}
