/**
 * @param {Repo} repo
 * @param {int} noticeId
 * @return {Notice|null}
 */
export default function findNoticeInRepo(repo, noticeId) {
    return repo.find(noticeId)
}
