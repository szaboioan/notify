export default class Array {

    /**
     * @param {Array}a
     * @param {Array}b
     * @returns {Array}
     */
    static diff(a, b) {
        return a.filter(x => !b.includes(x))
    }
}
