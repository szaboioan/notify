export class Notice {

    /**
     * @param {string}type
     * @param {string}text
     * @param {boolean}showCloseButton
     */
    constructor(type, text, showCloseButton) {
        this.id = 0;
        this.type = type
        this.text = text
        this.showCloseButton = showCloseButton
    }
}

/**
 * @param {string}type
 * @param {string}text
 * @param {boolean}showCloseButton
 */
export function makeNotice(type, text, showCloseButton) {
    return new Notice(type, text, showCloseButton);
}
