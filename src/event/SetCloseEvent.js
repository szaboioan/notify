/**
 * @param {Element}noticesContainer
 * @param {function}reactUi
 * @param {Repo}repo
 */
export default function setCloseEvent(noticesContainer, reactUi, repo) {
    noticesContainer.addEventListener('click', function (e) {
        let clicked = e.target
        if (clicked && clicked.matches(".toast-controls .toast-close")) {
            let toast = clicked.closest('.notify-item')
            let id = parseInt(toast.dataset.id)
            repo.remove(id)
            reactUi(repo.getNotices())
        }
    })
}
