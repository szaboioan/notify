import ArrayHelper from '../util/Array'
import {appendCounter, createCounter, removeItemAfter} from "./Counter"
import {createNoticeItem} from './NoticeHtmlItem'

class Ui {

    /**
     * @param {Element}container
     * @param {int}seconds
     */
    constructor(container, seconds) {
        this.container = container
        this.seconds = seconds
    }

    /**
     * @param {[Notice]}notices
     */
    addNotices(notices) {
        notices.forEach(notice => {
            let newItem = createNoticeItem(notice, 'div')
            this.container.appendChild(newItem)
            newItem.classList.add('active')
            if (this.seconds > 0) {
                let counter = createCounter()
                appendCounter(counter, newItem)
                removeItemAfter(counter, this.seconds, newItem)
            }
        })
    }

    /**
     * @returns {[int]}
     */
    getNoticesIdsPresentInDomAlready() {
        let notices = this.container.querySelectorAll('.notify-item')
        if (!notices.length) {
            return []
        }
        return Array.from(notices).map(notice => {
            return parseInt(notice.dataset.id)
        })
    }

    /**
     * @param {[Notice]}notices
     * @param {[int]}noticesInDomAlready
     * @returns {[Notice]}
     */
    getNoticesToAdd(notices, noticesInDomAlready) {
        return notices.filter(notice => {
            return noticesInDomAlready.indexOf(notice.id) === -1
        })
    }

    /**
     * @param {[int]}noticesInDomAlready
     * @param {[Notice]}notices
     * @return {[int]}
     */
    getNoticesToRemove(noticesInDomAlready, notices) {
        let noticesIds = notices.map(notice => notice.id)
        return ArrayHelper.diff(noticesInDomAlready, noticesIds)
    }

    /**
     * @param {[Notice]}notices
     */
    react(notices) {
        let noticesInDomAlready = this.getNoticesIdsPresentInDomAlready()
        let noticesToAdd = this.getNoticesToAdd(notices, noticesInDomAlready)
        let noticesToRemoveIds = this.getNoticesToRemove(noticesInDomAlready, notices)

        this.addNotices(noticesToAdd)
        this.removeNotices(noticesToRemoveIds)
    }

    /**
     * @param {[int]}noticesIds
     */
    removeNotices(noticesIds) {
        if (!noticesIds.length) {
            return
        }

        let selector = ''
        noticesIds.forEach(noticeId => {
            selector = selector + '.notify-item[data-id="' + noticeId + '"], '
        })

        let noticesInDomToRemove = document.querySelectorAll(selector.substr(0, selector.length - 2))
        noticesInDomToRemove.forEach(noticeToRemove => noticeToRemove.remove())
    }
}

let ui = null

/**
 * @param {Element}container
 * @param {int}seconds - Number of seconds after which notice will disappear.
 * @return {function}
 */
export function makeUi(container, seconds) {
    if (!ui) {
        ui = new Ui(container, seconds)
    }

    /**
     * @param {[Notice]}notices
     */
    return function (notices) {
        ui.react(notices)
    }
}
