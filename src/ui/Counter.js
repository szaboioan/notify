/**
 * @param {HTMLElement}counter
 * @param {HTMLElement}noticeItem
 */
export function appendCounter(counter, noticeItem) {
    noticeItem.querySelector('.toast-controls').appendChild(counter)
}

/**
 * @returns {HTMLElement}
 */
export function createCounter() {
    return document.createElement('span')
}

/**
 * @param {HTMLElement}counter
 * @param {int}seconds
 * @param {HTMLElement}newItem
 */
export function removeItemAfter(counter, seconds, newItem) {
    let second = 1
    let interval = setInterval(function () {
        updateCounter(counter, second)
        if (second > seconds) {
            newItem.remove()
            clearInterval(interval)
        }
        second++
    }, 1000)
}

/**
 * @param {HTMLElement}counter
 * @param {int}second
 */
export function updateCounter(counter, second) {
    counter.innerHTML = second + ''
}
