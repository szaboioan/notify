/**
 * @param {boolean}showCloseButton
 * @returns {string}
 */
function displayCloseButton(showCloseButton) {
    if (showCloseButton) {
        return `<button type="button" class="toast-close">
                x
            </button>`
    }

    return ``;
}

/**
 * @param {Notice}notice
 */
export default function getNoticeContent(notice) {
    return `<div class="toast toast-${notice.type}">
        <div class="toast-message">
            ${notice.text}
        </div>
        <div class="toast-controls">
            ${displayCloseButton(notice.showCloseButton)}
        </div>
        <div class="clear-both"></div>
    </div>`
}
