import getNoticeContent from "./template/notice-content"

/**
 * @param {Notice}notice
 * @param {string}tag
 * @return {HTMLElement}
 */
export function createNoticeItem(notice, tag) {
    let noticeItem = document.createElement(tag)
    noticeItem.classList.add('notify-item')
    noticeItem.dataset.id = notice.id
    noticeItem.innerHTML = getNoticeContent(notice)
    return noticeItem
}
